# Codogo Cosmic JS Blog Example App
## Building a super fast static site using AWS S3 and CloudFront

The example app to accompany a blog post.

Run using:
`yarn run start`
or
`npm run start`

Build using
`yarn run build`
or
`npm run build`